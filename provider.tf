terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.31.0"
    }
  }
}

provider "aws" {
  region                   = var.region
  shared_config_files      = ["/Users/deimos/.aws/conf"]
  shared_credentials_files = ["/Users/deimos/.aws/credentials"]
}
