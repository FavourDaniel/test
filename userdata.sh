#! /bin/bash
sudo apt update -y &&

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
# source ./install-runner.sh
sudo apt-get install gitlab-runner

sudo mkdir -p /etc/gitlab-runner/
sudo chown -R $USER:$USER /etc/gitlab-runner/
sudo chmod -R 755 /etc/gitlab-runner/
cd /etc/gitlab-runner/ 
touch config.toml

# Fetch the token from AWS Secrets Manager
# TOKEN=$(aws secretsmanager get-secret-value --secret-id gitlab-runner-token --query SecretString --output text)

sudo gitlab-runner register \
  --non-interactive \
  --config "/etc/gitlab-runner/config.toml" \
  --url "https://gitlab.com/" \
  # --token $TOKEN \
  --token glrt-HoL1ykWF4FyupgwtKRSQ\
  --executor "docker" \
  --docker-image alpine:latest
  
sudo systemctl enable gitlab-runner
sudo systemctl start gitlab-runner


## Install Docker
sudo apt-get install ca-certificates curl gnupg -y
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update -y

# Install latest version
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y

sudo usermod -aG docker $USER
sudo chmod 666 /var/run/docker.sock