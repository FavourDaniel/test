resource "aws_s3_bucket" "poc_bucket" {
  bucket = var.aws_s3_bucket_terraform
}

resource "aws_s3_bucket_server_side_encryption_configuration" "poc" {
  bucket = var.aws_s3_bucket_terraform

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.poc-bucket-key.arn
      sse_algorithm     = "aws:kms"
    }
  }
}


resource "aws_kms_key" "poc-bucket-key" {
  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 10
  enable_key_rotation     = true
}


resource "aws_kms_alias" "key-alias" {
  name          = "alias/poc-bucket-key"
  target_key_id = aws_kms_key.poc-bucket-key.key_id
}