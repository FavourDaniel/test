locals {
  name = "poc-test"
}

resource "aws_instance" "poc" {
    ami                    = var.ami
    instance_type          = var.instance_type
    key_name               = var.key_id
    vpc_security_group_ids = [aws_security_group.allow_ssh.id]
    user_data              = "${file("userdata.sh")}"

    tags = {
        Name = local.name
    }

}

data "aws_vpcs" "default" {}


resource "aws_security_group" "allow_ssh" {
    name        = "allow_ssh"
    description = "Allow SSH inbound traffic"

    ingress {
        description = "SSH from anywhere"
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port        = 0
        to_port          = 0
        protocol         = "-1"
        cidr_blocks      = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    }

    tags = {
        Name = "poc_sg"
    }
    }
