resource "aws_cloudwatch_metric_alarm" "cpu_utilization_high" {
    alarm_name        = "cpu-utilization-high"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = 1
    metric_name       = "CPUUtilization"
    namespace         = "AWS/EC2"
    period            = 60
    statistic         = "Average"
    threshold         = 10
    alarm_description = "This metric triggers when CPU utilization exceeds 10%"
    alarm_actions     = [aws_sns_topic.alarm.arn]
    dimensions = {
        InstanceId = var.instance_id
    }
}

resource "aws_sns_topic" "alarm" {
 name = "CloudWatch_Alarm_Topic"
}

resource "aws_sns_topic_subscription" "alarm_subscription" {
    topic_arn = aws_sns_topic.alarm.arn
    protocol = "email"
    endpoint = "kuberneteslinux@gmail.com"
}