variable "region" {
  type    = string
  default = "eu-west-1"
}

variable "ami" {
  type    = string
  default = "ami-0905a3c97561e0b69"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "aws_s3_bucket_terraform" {
  default = "my-poc-backend-bucket-test"
}

variable "key_id" {
  default = "poc-key"
}

variable "instance_id" {
  default= "i-0e803e4ba422bacda"
}